/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function personalInfo(){
		let name = prompt("Enter your full name.");
		let age = prompt("Enter your age.");
		let address = prompt("What city do you live in?")

		console.log("Hello, "+ name);
		console.log("You are " + age + " years old.");
		console.log("You live in " + address);
	};

	personalInfo();
/*

	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function topFiveBands(){
		console.log("1. Eraserheads");
		console.log("2. Rivermaya");
		console.log("3. Atheneaum");
		console.log("4. Stone Temple Pilots");
		console.log("5. Nirvana");
	};

	topFiveBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	

*/
	
	//third function here:

	function topFiveMovies(){
		let rating = "Rotten Tomatoes Rating: "
		console.log("1. Deadpool ");
		console.log(rating + "85%")
		console.log("2. Hook ");
		console.log(rating + "25%");
		console.log("3. Notting Hill ");
		console.log(rating + "84%");
		console.log("4. Bill & Ted Bogus Journey ");
		console.log(rating + "59%");
		console.log("5. The Mighty Ducks ");
		console.log(rating + "65%");

	}

	topFiveMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
	function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


/*console.log(friend1);
console.log(friend2);*/
	printFriends();